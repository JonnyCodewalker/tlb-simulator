CC=gcc
NAME=tlb
RELEASE_FLAGS= -std=c11 -O3 -o
DEBUG_FLAGS= -std=c11 -Wall -Wextra -Wunreachable-code -pedantic -ggdb -O0 -o
DEPS = src/*
debug: $(DEPS)
	$(CC) $(DEBUG_FLAGS) bin/$(NAME)-debug $(DEPS) &> compile.log
release: $(DEPS)
	$(CC) $(RELEASE_FLAGS) bin/$(NAME) $(DEPS) &> compile.log
clean:
	touch compile.log bin/rm && rm bin/* compile.log
