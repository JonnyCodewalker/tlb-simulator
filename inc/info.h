#ifndef INFO_H
#define INFO_H
#include "algorithms.h"
#include "tlb.h"
#define ANSI_COLOR_RED "\x1b[1;31m"
#define ANSI_COLOR_GREEN "\x1b[1;32m"
#define ANSI_COLOR_LIGHT_ORANGE "\x1b[33m"
#define ANSI_COLOR_LIGHT_GREEN "\x1b[32m"
#define ANSI_COLOR_RESET "\x1b[0m"
#define ANSI_COLOR_ORANGE "\x1b[1;33m"

void print_help(void);
void print_intro(void);

/**
 * Prints the current values stored in the tlb
 * @param tlb = pointer to the tlb to print
 * @param number = currenty number that is processed
 * @param attribute = wether or not to print the attribute from the union
 */
void print_TLB(TLB *tlb, unsigned size, unsigned number, bool attribute);
void print_misses(FLAGS flags, MC misses);
void print_numbers(unsigned *numbers, unsigned amount);

#endif
