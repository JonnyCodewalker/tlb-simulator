#include <stdint.h>
#include <stdbool.h>
#ifndef ALGO_H
#define ALGO_H
typedef struct Entry
{
  unsigned number;
  bool used;
} Entry;
typedef struct TLB
{
  Entry entry; //int entry; //TODO use unsigned int -> how to check if not used before?
  union {
    uint32_t age;
    uint32_t last_used;
    bool second_chance;
    uint32_t frequency;
  } u;
} TLB;

typedef struct MC
{
  long long fifo_miss;
  long long lru_miss;
  long long lfu_miss;
  long long sc_miss;
  long long random_miss;
  long long optimum_miss;
  long long wc_miss;
} MC;

/**
 * First in first out. When a TLB-Miss occurs the slot that has been 
 * populated the longest is freed up.
 * @param tlb = pointer to the TLB that is simulated
 * @param number = the current number to be processed
 * @param size = the size of the tlb, used for looping
 * @param cycle = the currenty cycle (aka how many numbers were already processed)
 * @param miss = pointer to the struct that is used for counting misses
 */
void fifo(TLB *tlb, unsigned number, unsigned size, unsigned cycle, MC *miss);

/**
 * Least recently used. When a TLB-Miss occurs the slot that has not been 
 * used for the longest time is freed up.
 * @param tlb = pointer to the TLB that is simulated
 * @param number = the current number to be processed
 * @param size = the size of the tlb, used for looping
 * @param cycle = the currenty cycle (aka how many numbers were already processed)
 * @param miss = pointer to the struct that is used for counting misses
 */
void lru(TLB *tlb, unsigned number, unsigned size, unsigned cycle, MC *miss);

/**
 * Least frequently used. When a TLB-Miss occurs the slot that has been 
 * used the least regularly is freed up.
 * @param tlb = pointer to the TLB that is simulated
 * @param number = the current number to be processed
 * @param size = the size of the tlb, used for looping
 * @param miss = pointer to the struct that is used for counting misses
 */
void lfu(TLB *tlb, unsigned number, unsigned size, MC *miss);

/**
 * Second chance. When a TLB-Miss occurs the slots get tested if they have been
 * used since the last miss. if yes they get flagged as no, if no they get replaced.
 * @param tlb = pointer to the TLB that is simulated
 * @param number = the current number to be processed
 * @param size = the size of the tlb, used for looping
 * @param miss = pointer to the struct that is used for counting misses
 * @param state = pointer to the int that stores wich slot was checked last
 */
void sc(TLB *tlb, unsigned number, unsigned size, MC *miss, int *state);

/**
 * Optimum. When a TLB-Miss occurs the slot that will not be 
 * used for the longest time is freed up.
 * @param tlb = pointer to the TLB that is simulated
 * @param number = the current number to be processed
 * @param size = the size of the tlb, used for looping
 * @param cycle = the currenty cycle (aka how many numbers were already processed)
 * @param amount = the total amount of numbers, used for lookahead
 * @param miss = pointer to the struct that is used for counting misses
 */
void optimum(TLB *tlb, unsigned *numbers, unsigned size, unsigned cycle, unsigned amount, MC *miss);

/**
 * Worst case. When a TLB-Miss occurs the slot that will not be 
 * used for the shortest time is freed up.
 * @param tlb = pointer to the TLB that is simulated
 * @param numbers = the current number to be processed
 * @param size = the size of the tlb, used for looping
 * @param cycle = the currenty cycle (aka how many numbers were already processed)
 * @param amount = the total amount of numbers, used for lookahead
 * @param miss = pointer to the struct that is used for counting misses
 */
void wc(TLB *tlb, unsigned *numbers, unsigned size, unsigned cycle, unsigned amount, MC *miss);

/**
 * Random. When a TLB-Miss occurs the slot that is to be replaced 
 * is selected randomly.
 * @param tlb = pointer to the TLB that is simulated
 * @param number = the current number to be processed
 * @param size = the size of the tlb, used for looping
 * @param miss = pointer to the struct that is used for counting misses
 */
void random(TLB *tlb, unsigned number, unsigned size, MC *miss);
#endif
