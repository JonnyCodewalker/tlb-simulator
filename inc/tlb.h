#include <stdbool.h>
#ifndef TLB_H
#define TLB_H
#include "algorithms.h"

typedef struct FLAGS
{
    bool verbose;
    bool run_fifo;
    bool run_lru;
    bool run_lfu;
    bool run_sc;
    bool run_optimum;
    bool run_wc;
    bool run_random;
    bool print_numbers;
    bool save;
    bool load;
    int repeats;
} FLAGS;

/**
 * Entry point, used for parsing input, basic input validation and flags as well 
 * as controlling program flow.
 * @param argc = amount of arguments given
 * @param argv = pointer to the arguments
 */
int main(int argc, char *argv[]);

/**
 * seed random numbers
 * @param amount = the amount of numbers
 * @param limit = the range of numbers (0-limit)
 * @param numbers = pointer to where to store the numbers 
 */
void seed(unsigned amount, unsigned limit, unsigned *numbers);

/**
 * Simulator that goes through the numbers and simulates the TLBs
 * @param amount = the amount of numbers, used for looping
 * @param size = the size of a tlb, used for malloc & the tlbs
 * @param flags = the configuration flags.
 */
void simulator(unsigned amount, unsigned limit, unsigned size, FLAGS flags);

/**
 * initialize a TLB, zeroing the entry and setting the union to -1
 * @param tlb = a pointer to the tlb to initialize
 * @param size = size of the TLB, used for looping
 */
void init(TLB *tlb, unsigned size);
/**
 * @brief save the numbers to the file numbers.txt
 * @param numbers = the list of numbers to save
 * @param amount = the amount of numbers to save
 */
void saveNumbers(unsigned *numbers, unsigned amount, unsigned limit, unsigned size);
void loadNumbers(unsigned *numbers, unsigned *amount, unsigned *limit, unsigned *size);

#endif
