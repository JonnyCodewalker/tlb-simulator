# Translation Lookaside Buffer Simulator

#### Made by Jonathan Schaefer

You can run the tlb with these flags:
```./tlb --all -a [amount] -l [limit] -s [size] ```

to run the Simulator with amount, limit, size and all algorithms.

**Required arguments are**: 

`-a`[1-maxint] for the amount of numbers 

`-l` [1-maxint] for the range of numbers 

`-s` [1-maxint] for the size of the tlb 

**Optional arguments are:** 

`--verbose` for printing tlb each cycle 
   (currently only works when only one algorithm is run) 

`--fifo` for running the  first in first out algorithm 

`--lru` for running the least recently used algorithm 

`--lfu` for running the least frequently used algorithm 

`--sc` for running the second chance algorithm 

`--optimum` for running the optimal algorithm 

`--wc` for running the worst case algorithm 

`--random` for tunning the random algorithm 

`--all` to run all algorithms 

`--help` to show the help 

-----------------------

License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
