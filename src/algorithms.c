#include <stdbool.h>
#include <stdlib.h>
#include "../inc/algorithms.h"

void fifo(TLB *tlb, unsigned number, unsigned size, unsigned cycle, MC *miss)
{
  unsigned oldest = 0;
  bool missed = true;
  for (unsigned index = 0; index < size; index++)
  {
    if (tlb[index].entry.number == number || tlb[index].entry.used == false)
    {
      tlb[index].entry.number = number;
      tlb[index].entry.used = true;
      tlb[index].u.age = cycle;
      missed = false;
      break;
    }
    if (tlb[index].u.age <= tlb[oldest].u.age)
    {
      oldest = index;
    }
  }
  if (missed)
  {
    miss->fifo_miss++;
    tlb[oldest].entry.number = number;
    tlb[oldest].u.age = cycle;
  }
}

void lru(TLB *tlb, unsigned number, unsigned size, unsigned cycle, MC *miss)
{
  unsigned lru = 0;
  bool missed = true;

  for (unsigned index = 0; index < size; index++)
  {
    if (tlb[index].entry.number == number || tlb[index].entry.used == false)
    {
      tlb[index].entry.number = number;
      tlb[index].entry.used = true;
      tlb[index].u.last_used = cycle;
      missed = false;
      break;
    }
    if (tlb[index].u.last_used < tlb[lru].u.last_used)
    {
      lru = index;
    }
  }
  if (missed)
  {
    miss->lru_miss++;
    tlb[lru].entry.number = number;
    tlb[lru].u.last_used = cycle;
  }
}

void lfu(TLB *tlb, unsigned number, unsigned size, MC *miss)
{
  unsigned lfu = 0;
  bool missed = true;

  for (unsigned index = 0; index < size; index++)
  {
    if ((tlb[index].entry.number == number || tlb[index].entry.used == false) && missed) //so that it only gets added 1 time
    {
      tlb[index].entry.number = number;
      tlb[index].entry.used = true;
      tlb[index].u.frequency = ~(~tlb[index].u.frequency >> 1);
      missed = false;
    }
    else
    {
      tlb[index].u.frequency = tlb[index].u.frequency >> 1;
    }
    if (tlb[0].u.frequency < tlb[lfu].u.frequency)
    {
      lfu = index;
    }
  }
  if (missed)
  {
    miss->lfu_miss++;
    tlb[lfu].entry.number = number;
    //since we already shifted every entry we now have to flip the highest bit
    tlb[lfu].u.frequency = tlb[lfu].u.frequency | 0x70000000;
  }
}

void sc(TLB *tlb, unsigned number, unsigned size, MC *miss, int *state)
{
  bool missed = true;
  for (unsigned index = 0; index < size; index++)
  {
    if (tlb[index].entry.number == number || tlb[index].entry.used == false)
    {
      tlb[index].entry.number = number;
      tlb[index].entry.used = true;
      tlb[index].u.second_chance = true;
      missed = false;
      break;
    }
  }
  if (missed)
  {
    bool done = false;
    miss->sc_miss++;
    while (!done)
    {
      for (unsigned index = *state; index < size; index++)
      {
        if (!tlb[index].u.second_chance)
        {
          tlb[index].entry.number = number;
          tlb[index].u.second_chance = true;
          *state = index;
          done = true;
          break;
        }
        else
        {
          tlb[index].u.second_chance = false;
        }
      }
      for (int index = 0; index < *state; index++)
      {
        if (!tlb[index].u.second_chance)
        {
          tlb[index].entry.number = number;
          tlb[index].u.second_chance = true;
          *state = index;
          done = true;
          break;
        }
        else
        {
          tlb[index].u.second_chance = false;
        }
      }
    }
  }
}

void random(TLB *tlb, unsigned number, unsigned size, MC *miss)
{
  bool missed = true;
  for (unsigned index = 0; index < size; index++)
  {
    if (tlb[index].entry.number == number || tlb[index].entry.used == false)
    {
      tlb[index].entry.number = number;
      tlb[index].entry.used = true;
      missed = false;
      break;
    }
  }
  if (missed)
  {
    miss->random_miss++;
    tlb[rand() % size].entry.number = number;
  }
}

void optimum(TLB *tlb, unsigned *numbers, unsigned size, unsigned cycle, unsigned amount, MC *miss)
{
  bool missed = true;
  for (unsigned index = 0; index < size; index++)
  {
    if (tlb[index].entry.number == *(numbers + cycle) || tlb[index].entry.used == false)
    {
      tlb[index].entry.number = *(numbers + cycle);
      tlb[index].entry.used = true;
      missed = false;
      break;
    }
  }
  if (missed)
  {
    int next[size];
    for (unsigned index = 0; index < size; index++)
    {
      next[index] = amount - cycle; //it doesn't appear, so give it highest value
      for (int i = 1; (i + cycle) < (amount); i++)
      {
        if (tlb[index].entry.number == *(numbers + (i + cycle)))
        {
          next[index] = i;
          break;
        }
      }
    }
    //get highest number from next
    int highest = 0;
    for (unsigned i = 0; i < size; i++)
    {
      if (next[i] > next[highest])
      {
        highest = i;
      }
    }
    //replace tlb because that is the one that isnt gonna be used for the longest
    tlb[highest].entry.number = *(numbers + cycle);
    miss->optimum_miss++;
  }
}

void wc(TLB *tlb, unsigned *numbers, unsigned size, unsigned cycle, unsigned amount, MC *miss)
{
  bool missed = true;
  for (unsigned index = 0; index < size; index++)
  {
    if (tlb[index].entry.number == *(numbers + cycle) || tlb[index].entry.used == false)
    {
      tlb[index].entry.number = *(numbers + cycle);
      tlb[index].entry.used = true;
      missed = false;
      break;
    }
  }
  if (missed)
  {
    unsigned next[size];
    for (unsigned index = 0; index < size; index++)
    {
      next[index] = amount - cycle; //it doesn't appear, so give it highest value
      for (unsigned i = 1; (i + cycle) < (amount); i++)
      {
        if (tlb[index].entry.number == *(numbers + (i + cycle)))
        {
          next[index] = i;
          break;
        }
      }
    }
    //get lowest number from next
    unsigned lowest = 0;
    for (unsigned i = 0; i < size; i++)
    {
      if (next[i] < next[lowest])
      {
        lowest = i;
      }
    }
    //replace tlb because that is the one that is gonna be used again the fastest
    tlb[lowest].entry.number = *(numbers + cycle);
    miss->wc_miss++;
  }
}
