#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "../inc/tlb.h"
#include "../inc/info.h"
#ifndef _INCL_GUARD
#define _INCL_GUARD
#include "../inc/algorithms.h"
#endif

int main(int argc, char *argv[])
{
  srand(time(NULL)); //seed numbers
  if (argc == 1)
  {
    print_intro();
    exit(0);
  }
  unsigned amount = 0;
  unsigned limit = 0;
  unsigned size = 0;
  unsigned algos = 0;
  FLAGS flags = {0};
  flags.repeats = 0;
  //parse all the input
  for (int i = 1; i < argc; i++)
  {
    if (strcmp(argv[i], "--help") == 0)
    {
      print_help();
      exit(0);
    }
    else if (strcmp(argv[i], "-a") == 0)
    {
      int temp = atoi(argv[++i]);
      amount = temp < 0 ? 0 : temp;
    }
    else if (strcmp(argv[i], "-l") == 0)
    {
      int temp = atoi(argv[++i]);
      limit = temp < 0 ? 0 : temp;
    }
    else if (strcmp(argv[i], "-s") == 0)
    {
      int temp = atoi(argv[++i]);
      size = temp < 0 ? 0 : temp;
    }
    else if (strcmp(argv[i], "--verbose") == 0)
    {
      flags.verbose = true;
    }
    else if (strcmp(argv[i], "--fifo") == 0)
    {
      flags.run_fifo = true;
      algos++;
    }
    else if (strcmp(argv[i], "--lru") == 0)
    {
      flags.run_lru = true;
      algos++;
    }
    else if (strcmp(argv[i], "--lfu") == 0)
    {
      flags.run_lfu = true;
      algos++;
    }
    else if (strcmp(argv[i], "--sc") == 0)
    {
      flags.run_sc = true;
      algos++;
    }
    else if (strcmp(argv[i], "--optimum") == 0)
    {
      flags.run_optimum = true;
      algos++;
    }
    else if (strcmp(argv[i], "--wc") == 0)
    {
      flags.run_wc = true;
      algos++;
    }
    else if (strcmp(argv[i], "--random") == 0)
    {
      flags.run_random = true;
      algos++;
    }
    else if (strcmp(argv[i], "--numbers") == 0)
    {
      flags.print_numbers = true;
    }
    else if (strcmp(argv[i], "--all") == 0)
    {
      flags.verbose = false;
      flags.run_fifo = true;
      flags.run_lru = true;
      flags.run_lfu = true;
      flags.run_sc = true;
      flags.run_optimum = true;
      flags.run_wc = true;
      flags.run_random = true;
      algos += 2;
    }
    else if (strcmp(argv[i], "--save") == 0)
    {
      flags.save = true;
    }
    else if (strcmp(argv[i], "--load") == 0)
    {
      flags.load = true;
    }
    else if (strcmp(argv[i], "-r") == 0)
    {
      int temp = atoi(argv[++i]);
      flags.repeats = temp < 0 ? 0 : temp;
    }
    else
    {
      printf(ANSI_COLOR_RED "Error: " ANSI_COLOR_RESET "\"%s\" is not a recognized command\n", argv[i]);
      exit(1);
    }
  }
  if (algos > 1 && flags.verbose)
  {
    printf(ANSI_COLOR_LIGHT_ORANGE "Warning: More than one algorithm selected, \"--verbose\" is ignored\n" ANSI_COLOR_RESET);
    flags.verbose = false;
  }

  if (flags.repeats && flags.load)
  {
    flags.repeats = 0;
    printf(ANSI_COLOR_LIGHT_ORANGE "Warning: Numbers are loaded, repetitions are being ignored.\n" ANSI_COLOR_RESET);
  }
  if (flags.load)
  {
    if (amount || limit || size)
      printf(ANSI_COLOR_LIGHT_ORANGE "Warning: Numbers are loaded, \"-a\" \"-s\" and \"-l\" are being ignored.\n" ANSI_COLOR_RESET);
  }
  else if (!amount || !limit || !size)
  {
    printf(ANSI_COLOR_RED "Error: " ANSI_COLOR_RESET "amount, limit & size have to be a natural number\n");
    exit(1);
  }

  if (limit <= size)
  {
    printf(ANSI_COLOR_RED "Error: " ANSI_COLOR_RESET "No misses can occur since the range is smaller or equal to the TLB-Size\n");
    exit(0);
  }
  if (amount <= size)
  {
    printf(ANSI_COLOR_RED "Error: " ANSI_COLOR_RESET "No misses can occur since the amount is smaller or equal to the TLB-Size\n");
    exit(0);
  }
  if (flags.save && flags.load)
  {
    printf(ANSI_COLOR_RED "Error: " ANSI_COLOR_RESET "Saving and loading numbers at the same time makes no sense and is not possible\n");
    exit(0);
  }
  simulator(amount, limit, size, flags);
  return 0;
}

void simulator(unsigned amount, unsigned limit, unsigned size, FLAGS flags)
{
  //all the malloc shit
  unsigned *numbers = malloc(sizeof(int) * amount);
  TLB *fifo_tlb = malloc(sizeof(TLB) * size);
  TLB *lru_tlb = malloc(sizeof(TLB) * size);
  TLB *lfu_tlb = malloc(sizeof(TLB) * size);
  TLB *sc_tlb = malloc(sizeof(TLB) * size);
  TLB *random_tlb = malloc(sizeof(TLB) * size);
  TLB *optimum_tlb = malloc(sizeof(TLB) * size);
  TLB *wc_tlb = malloc(sizeof(TLB) * size);
  if (!numbers || !fifo_tlb || !lru_tlb || !lfu_tlb || !sc_tlb || !random_tlb || !optimum_tlb || !wc_tlb)
  {
    printf(ANSI_COLOR_RED "Error: " ANSI_COLOR_RESET "unsucessful memory allocation\n");
    exit(1);
  }

  int state = 0;
  //inizialization of the translation lookaside buffers
  init(fifo_tlb, size);
  init(lru_tlb, size);
  init(lfu_tlb, size);
  init(sc_tlb, size);
  init(random_tlb, size);
  init(optimum_tlb, size);
  init(wc_tlb, size);

  //initialization of the TLB-Miss counters && seeding of numbers
  MC misses = {0};
  if (flags.load)
  {
    free(numbers);
    loadNumbers(numbers, &amount, &limit, &size);
  }
  else
  {
    seed(amount, limit, numbers);
  }

  if (flags.save)
    saveNumbers(numbers, amount, limit, size);

  //actual simulation
  unsigned number;
  int repeats = 0;
  do
  {
    if (flags.print_numbers)
      print_numbers(numbers, amount);

    for (unsigned cycle = 0; cycle < amount; cycle++)
    {
      number = *(numbers + cycle);
      if (flags.run_random)
      {
        random(random_tlb, number, size, &misses);
        if (flags.verbose)
        {
          print_TLB(random_tlb, size, number, false);
        }
      }

      if (flags.run_fifo)
      {
        fifo(fifo_tlb, number, size, cycle, &misses);
        if (flags.verbose)
        {
          print_TLB(fifo_tlb, size, number, true);
        }
      }

      if (flags.run_lru)
      {
        lru(lru_tlb, number, size, cycle, &misses);
        if (flags.verbose)
        {
          print_TLB(lru_tlb, size, number, true);
        }
      }
      if (flags.run_lfu)
      {
        lfu(lfu_tlb, number, size, &misses);
        if (flags.verbose)
        {
          print_TLB(lfu_tlb, size, number, true);
        }
      }

      if (flags.run_sc)
      {
        sc(sc_tlb, number, size, &misses, &state);
        if (flags.verbose)
        {
          print_TLB(sc_tlb, size, number, true);
        }
      }

      if (flags.run_optimum)
      {
        optimum(optimum_tlb, numbers, size, cycle, amount, &misses);
        if (flags.verbose)
        {
          print_TLB(optimum_tlb, size, number, false);
        }
      }

      if (flags.run_wc)
      {
        wc(wc_tlb, numbers, size, cycle, amount, &misses);
        if (flags.verbose)
        {
          print_TLB(wc_tlb, size, number, false);
        }
      }
    }
    repeats++;
    seed(amount, limit, numbers);
  } while (repeats < flags.repeats);
  //output
  if (flags.repeats) //only needed if more than 1 go through
  {
    misses.fifo_miss /= flags.repeats;
    misses.lfu_miss /= flags.repeats;
    misses.lru_miss /= flags.repeats;
    misses.optimum_miss /= flags.repeats;
    misses.random_miss /= flags.repeats;
    misses.sc_miss /= flags.repeats;
    misses.wc_miss /= flags.repeats;
    printf("Average misses on %d runs. Integer rounding applies.\n", flags.repeats);
  }

  print_misses(flags, misses);

  free(numbers);
  free(fifo_tlb);
  free(lru_tlb);
  free(lfu_tlb);
  free(random_tlb);
  free(sc_tlb);
  free(optimum_tlb);
  free(wc_tlb);
}

void seed(unsigned amount, unsigned limit, unsigned *numbers)
{
  for (; amount-- > 0;)
    *(numbers + amount) = rand() % limit;
}

void init(TLB *tlb, unsigned size)
{
  for (unsigned i = 0; i < size; i++)
  {
    tlb[i].entry.number = 0;
    tlb[i].entry.used = false;
    tlb[i].u.age = 0;
  }
}

void saveNumbers(unsigned *numbers, unsigned amount, unsigned limit, unsigned size)
{
  FILE *f = fopen("numbers.txt", "w");
  if (f == NULL)
  {
    printf("Error opening file!\n");
    exit(1);
  }
  fprintf(f, "%u\n", amount);
  fprintf(f, "%u\n", limit);
  fprintf(f, "%u\n", size);
  for (unsigned number, i = 0; i < amount; i++)
  {
    number = numbers[i];
    fprintf(f, "%u\n", number);
  }
  fclose(f);
}

//FIXME loading doesn't work, doesn't read the numbers :<
void loadNumbers(unsigned *numbers, unsigned *amount, unsigned *limit, unsigned *size)
{
  FILE *f = fopen("numbers.txt", "r");
  if (f == NULL)
  {
    printf("Error opening file!\n");
    exit(1);
  }
  int c = getc(f);
  while (c != EOF)
  {
    if (c == '\n')
    {
      (*amount)++;
    }
    c = getc(f);
  }
  (*amount) -= 3; //-3 since amount, limit, size are also stored in the file
  char line[256];

  numbers = malloc(sizeof(int) * (*amount));
  fgets(line, sizeof(line), f);
  line[strcspn(line, "\n")] = 0;
  *amount = (unsigned)atol(line);
  line[strcspn(line, "\n")] = 0;
  fgets(line, sizeof(line), f);
  *limit = (unsigned)atol(line);
  line[strcspn(line, "\n")] = 0;
  fgets(line, sizeof(line), f);
  *size = (unsigned)atol(line);
  for (unsigned i = 0; i < (*amount); i++)
  {
    fgets(line, sizeof(line), f);
    line[strcspn(line, "\n")] = 0;
    numbers[i] = (unsigned)atol(line);
  }
  fclose(f);
}