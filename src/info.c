#include <stdio.h>
#include <stdlib.h>
#include "../inc/info.h"

void print_help(void)
{
  printf("\
                TLB Simulator\n \
  ----------------------- \n \
  required arguments are: \n \
  -a [1-maxint] for the " ANSI_COLOR_GREEN "amount" ANSI_COLOR_RESET " of numbers \n \
  -l [1-maxint] for the " ANSI_COLOR_GREEN "range" ANSI_COLOR_RESET " of numbers \n \
  -s [1-maxint] for the " ANSI_COLOR_GREEN "size" ANSI_COLOR_RESET " of the tlb \n \
  ----------------------- \n \
  optional arguments are: \n \
  --verbose for printing tlb each cycle \n \
      " ANSI_COLOR_LIGHT_ORANGE "(currently only works when only one algorithm is run)" ANSI_COLOR_RESET " \n \
  --fifo for running the " ANSI_COLOR_GREEN " first in first out" ANSI_COLOR_RESET " algorithm \n \
  --lru for running the " ANSI_COLOR_GREEN "least recently used" ANSI_COLOR_RESET " algorithm \n \
  --lfu for running the " ANSI_COLOR_GREEN "least frequently used" ANSI_COLOR_RESET " algorithm \n \
  --sc for running the " ANSI_COLOR_GREEN "second chance" ANSI_COLOR_RESET " algorithm \n \
  --optimum for running the " ANSI_COLOR_GREEN "optimal" ANSI_COLOR_RESET " algorithm \n \
  --wc for running the " ANSI_COLOR_GREEN "worst case" ANSI_COLOR_RESET " algorithm \n \
  --random for tunning the " ANSI_COLOR_GREEN "random" ANSI_COLOR_RESET " algorithm \n \
  --all to run all algorithms \n \
  --numbers to print the numbers prior to executing\n \
  --help to show this help \n \
  ----------------------- \n");
}

void print_intro(void)
{
  printf("\
Translation Lookaside Buffer Simulator\n\
     Made by Jonathan Schaefer\n\
     -------------------------\n\
     \n\
Use \"--help\" to view all available commands.\n\
" ANSI_COLOR_GREEN "./tlb --all -a [amount] -l [limit] -s [size]" ANSI_COLOR_RESET "\n\
to run the Simulator with amount, limit,\n\
size and all algorithms.\n\
");
}

void print_TLB(TLB *tlb, unsigned size, unsigned number, bool attribute)
{
  printf("%u|||", number);
  for (unsigned index = 0; index < size; index++)
  {
    if (attribute)
    {
      printf(" %u[%u] |", tlb[index].entry.number, tlb[index].u.age);
    }
    else
    {
      printf(" %u |", tlb[index].entry.number);
    }
  }
  printf("\n");
}

void print_misses(FLAGS flags, MC misses)
{
  if (flags.run_optimum)
  {
    if (misses.optimum_miss)
    {
      printf("Algorithm   Misses    %% of optimum\n");
      printf("----------------------------------\n");
      if (flags.run_fifo)
        printf("fifo        %10lld    %3.2f%%\n", misses.fifo_miss, 100.0 * misses.fifo_miss / misses.optimum_miss);
      if (flags.run_lfu)
        printf("lfu         %10lld    %3.2f%%\n", misses.lfu_miss, 100.0 * misses.lfu_miss / misses.optimum_miss);
      if (flags.run_lru)
        printf("lru         %10lld    %3.2f%%\n", misses.lru_miss, 100.0 * misses.lru_miss / misses.optimum_miss);
      if (flags.run_random)
        printf("random      %10lld    %3.2f%%\n", misses.random_miss, 100.0 * misses.random_miss / misses.optimum_miss);
      if (flags.run_sc)
        printf("sc          %10lld    %3.2f%%\n", misses.sc_miss, 100.0 * misses.sc_miss / misses.optimum_miss);
      if (flags.run_optimum)
        printf("optimum     %10lld    %3.2f%%\n", misses.optimum_miss, 100.0 * misses.optimum_miss / misses.optimum_miss);
      if (flags.run_wc)
        printf("wc          %10lld    %3.2f%%\n", misses.wc_miss, 100.0 * misses.wc_miss / misses.optimum_miss);
    }
    else
    {
      printf(ANSI_COLOR_LIGHT_ORANGE "No misses occured!\n" ANSI_COLOR_RESET);
    }
  }
  else
  {
    printf("------------------\n");
    printf("Algorithm   Misses\n");
    printf("------------------\n");
    if (flags.run_fifo)
      printf("fifo        %10lld    \n", misses.fifo_miss);
    if (flags.run_lfu)
      printf("lfu         %10lld    \n", misses.lfu_miss);
    if (flags.run_lru)
      printf("lru         %10lld    \n", misses.lru_miss);
    if (flags.run_random)
      printf("random      %10lld    \n", misses.random_miss);
    if (flags.run_sc)
      printf("sc          %10lld    \n", misses.sc_miss);
    if (flags.run_wc)
      printf("wc          %10lld    \n", misses.wc_miss);
  }
}

void print_numbers(unsigned *numbers, unsigned amount)
{
  printf("Numbers: \n");
  for (unsigned i = 0; i < amount; i++)
  {
    printf(" %u", *(numbers + i));
  }
  printf("\n------------------ \n");
}